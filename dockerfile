FROM alpine     

RUN apk update && apk add --no-cache nodejs-current nodejs-npm vim

RUN mkdir -p /usr/src/app   
WORKDIR /usr/src/app                        

COPY . .

RUN npm install

EXPOSE 8216

ENTRYPOINT ["/bin/sh","-c"]

CMD ["npm start"] 

